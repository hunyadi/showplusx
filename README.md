# showplusx #

showplusx is a slideshow engine that aims to illustrate how to produce sleek animations using plain JavaScript and mostly CSS3. It borrows from traditional [showplus](http://hunyadi.info.hu/levente/en/showplus) and classical [Slideshow2](http://www.electricprism.com/aeron/slideshow/) but does not depend on third-party JavaScript libraries.

Features:

* Designed for modern browsers, including Chrome, Edge, Firefox and Safari.
* Fully responsive, scales to arbitrary size, over-sized images are automatically down-scaled.
* Performance-optimized, uses browser features (not JavaScript) for animation.
* Appearance tailored with CSS only.
* Simple API and out-of-the-box snippets how to get it working on a webpage.
* 100% CSS and pure JavaScript (ECMAScript 5 in minified output). No dependencies on third-party JavaScript libraries.
* Small network footprint.

### Examples ###

* [Live demo](http://hunyadi.info.hu/projects/showplusx/)

### Related work ###

* The [original showplus](http://hunyadi.info.hu/levente/en/showplus)