// make page header remain constantly visible
(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        var header = $.getElementById('header');
        header.style.position = 'fixed';  // make header constantly visible
        function adjustOffsetMargin() {  // calculate proper offset margin for main content area
            $.getElementById('main').style.marginTop = header.offsetHeight + 'px';
        };
        adjustOffsetMargin();  // calculate when page layout is available
        window.addEventListener('resize', adjustOffsetMargin);  // re-calculate on resize
    });
})(document);
