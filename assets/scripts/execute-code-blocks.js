// execute all JavaScript code blocks in the document
document.addEventListener('DOMContentLoaded', function () {
    [].forEach.call(document.body.querySelectorAll('pre > code.javascript'), function (block) {
        var s = document.createElement('script');
        var code = '(function() {' + block.innerText + '}).call(window);';
        s.src = 'data:text/javascript;charset=' + (document.characterSet || 'utf-8') + ',' + encodeURIComponent(code);
        document.body.appendChild(s);
    });
});
