//
// Minifies CSS files, replacing relative URLs with inline assets
//
// Requirements:
// npm install cssnano
// npm install postcss-assets
//

var postcss = require('postcss');
var assets = require('postcss-assets');
var cssnano = require('cssnano')({
    reduceTransforms: false
});
var fs = require('fs');

fs.readFile('showplusx.css', 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }

    // postcss-assets recognizes syntax inline(...) for image inlining as base64 but not url(...)
    data = data.replace(/url\(([^()]+)\)/g, 'inline($1)');

    // minify CSS
    postcss([assets, cssnano])
        .process(data, { from: 'showplusx.css', to: 'showplusx.min.css' })
        .then(function (result) {
            fs.writeFile('showplusx.min.css', result.css, function(err) {
                if (err) {
                    return console.log(err);
                }
            });
        });
});
