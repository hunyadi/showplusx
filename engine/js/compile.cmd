@echo off
java -jar closure-compiler-v20171023.jar --compilation_level ADVANCED_OPTIMIZATIONS --js showplusx.js --js_output_file showplusx.min.js --language_in ECMASCRIPT6_STRICT --language_out ECMASCRIPT5_STRICT --output_wrapper ";(function() {%%output%%}).call(window);" --warning_level VERBOSE --jscomp_warning=reportUnknownTypes
